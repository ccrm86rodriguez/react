import { API_END_POINT }  from "../config"

export const fetchCharacters = () => {  
    return (
      fetch(`${API_END_POINT}/api/character/`, {
        method: 'get',
      })
      .then(response => response.json())
      .then(({results}) => {          
        return results
      })
    )
}

export const fetchCharacter = (id) => {
    return (
        fetch(`${API_END_POINT}/api/character/${id}`, {
            method: 'get',
        })
        .then(response => response.json())
        .then((character) => {
            return character            
        })
    )
}
  