import React, { Component } from "react";
import { Link } from 'react-router-dom'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';



class Characters extends Component {
    
    render() {
        const { characters } = this.props;        
        const Listcharacters = characters.map((character) => {
            return (
                <Grid item key={character.id}>
                    <Link to={`character/${character.id}`}>
                        <div className="character-item">
                            <img alt={character.name} src={character.image}/>
                            <p>{character.name}</p>
                            <p>{character.gender}</p>
                        </div>
                    </Link>    
                </Grid>     
            )
        })
        return (
            <main>
                <Typography variant="h2" color="inherit" noWrap>
                    Rick And Morty API
                </Typography>
                <Grid container spacing={16} justify="center">
                    { Listcharacters }
                </Grid>
            </main>
        )
      }
}

export default Characters