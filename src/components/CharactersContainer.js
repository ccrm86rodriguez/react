import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import Characters from "./Characters";
import { fetchCharacters } from "../api/characters";

class CharactersContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      characters: []
    };
  }

  componentDidMount() {
    fetchCharacters()
      .then(characters => {        
        this.setState({ characters });
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {    
    const { characters } = this.state;

    return <Characters characters={characters}/>;
  }
}

export default withRouter(CharactersContainer);
