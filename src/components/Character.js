import React, { Component } from 'react'

class Character extends Component {
    
    render() {
        const { character } = this.props        
        return (                        
            <div className="character-item">
                <img alt={character.name} src={character.image}/>
                <p>{character.name}</p>
                <p>{character.gender}</p>
            </div>            
        )
      }
}

export default Character