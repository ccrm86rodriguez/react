import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import Character from "./Character";
import { fetchCharacter } from "../api/characters";

class CharacterContainer extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      character: {}
    };
  }

  getCharacterId = () => {
    const { match } = this.props
    const { id } = match.params 
    return id
  }

  componentDidMount() {
    fetchCharacter(this.getCharacterId())
      .then(character => { 
        console.log(character)       
        this.setState({character});
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {    
  
    return <Character character={this.state.character}/>;
  }
}

export default withRouter(CharacterContainer);
