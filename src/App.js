import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import './App.css';
import CharactersContainer from './components/CharactersContainer'
import CharacterContainer from './components/CharacterContainer';

const App = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={CharactersContainer} />          
      <Route exact path="/character/:id" component={CharacterContainer} />          
    </Switch>
  </Router>
)

export default App;
